
var express = require('express');

var app = express();

var cluster = require('cluster');
var numCPUs = require('os').cpus().length;

var bodyParser = require('body-parser');
var urlencode = bodyParser.urlencoded({extended : false});

var redis = require('redis');
var redisClient = redis.createClient();

let count = 0;

exports.ping = (req,res) => {
	console.log("-------- from server 2 ",count);
	count++;
	res.send("from nginx server");
};

app.get('/ping',exports.ping);


app.listen(5555, () => {
	console.log("nginx server 2 start ");
});