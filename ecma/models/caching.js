var redis = require('redis');
var client = redis.createClient();
var async = require('async');

var caching = {};

caching.get_first_level_cache = function (store_id,callback) {
	var key= "first_level_"+store_id;
	client.hgetall(key,function(err,response){
		if(!err){
			callback(null,response);
		}else{
			callback(err);
		}
	});	
}

caching.get_second_level_cache = function(store_id,callback){
	var key = "second_level_" + store_id + "_collection_*";
	var responses = [];
	client.keys(key,function(err,collections){
		if(!err){
			async.eachSeries(collections,function(collection,callback){
				client.get(key,function(err,response){
					if(!err){
						//callback(null,response);
						responses.push(response);
					}else{
						callback(err);
					}
				});				
			},function(err){
				if(!err){
					callback(null,responses);
				}else{
					callback(err);
				}
			});
		}else{
			callback(err);
		}
	});
}

caching.update_second_level_cache = function(store_id,collection_name,update_collection,callback){
	var key = "second_level_"+store_id+"_collection_"+collection_name;
	client.set(key,update_collection,function(err,response){
		if(!err){
			callback(null,response);
		}else{
			callback(err);
		}
	});
}

module.exports = caching;
