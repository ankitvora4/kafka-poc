var kafka_helper = (function (argument) {

	var kafka = require('kafka-node');
	var Producer = kafka.Producer;
	var client = new kafka.Client();


	// var partitioner = RoundRobinPartitioner(partitions=[
	//     TopicPartition(topic='topic', partition=0),
	//     TopicPartition(topic='topic', partition=1)
	// ])

	var producer = new Producer(client, {partitionerType: 2});

	producer.on('ready',function(err,result){
		if(!err){
			console.log("--------------->> kafka producer is up and running");	
		}else{
			throw err;
		}
	});

	producer.on('error',function(err){
		console.log("----------------------------------------------------");
		throw err;
	});

	return producer;

})();


module.exports = kafka_helper;