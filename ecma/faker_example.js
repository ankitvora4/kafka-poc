let express = require('express');
let faker_app = express();

let fs = require('fs');

let faker = require('faker');

faker_app.get('/dummy_request/:count',(req,res) => {
	

	// fs.writeFile('bulkRequest.txt', str ,(err) => {
	// 	if(err){
	// 		throw err;
	// 	}else{
	// 		res.send("ok");
	// 	}
	// });
	
	let fd;
	let name, email, str; 
	let count = req.params.count;
	try {
	  fd = fs.openSync('bulkRequest.txt', 'a');
	  for (var i = 0; i < count; i++) {
		name = faker.name.findName();
		email = faker.internet.email();
		str = JSON.stringify( { "name" : name , "email" : email } );
	  	
	  	console.log(i);
	  	fs.appendFileSync(fd, str);
	  }  
	} catch (err) {
	  throw err;
	} finally {
	  if (fd !== undefined)
	    fs.closeSync(fd);
		res.send("ok");
	}
	
});


faker_app.listen(9999, () => {
	console.log("faker has been started on port number 9999");
});