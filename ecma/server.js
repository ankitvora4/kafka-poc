var express = require('express');

var app = express();

var cluster = require('cluster');
var numCPUs = require('os').cpus().length;

var bodyParser = require('body-parser');
var urlencode = bodyParser.urlencoded({extended : false});

var redis = require('redis');
var redisClient = redis.createClient();
var singleton_module = require('./controllers/singleton.js');
//redisClient.select((process.env.NODE_ENV || 'development').length);

var kafka_producer_helper = require('./helper/kafka_helper.js');
//var kafka_consumer_helper = require('./helper/kafka_consumer_helper.js');
var subscriber = redis.createClient();
//console.log(JSON.stringify(kafka_consumer_helper));

let count = 0;

// exports.nvm_latest = function(req,res){
// 	res.send("ok");
// }

// exports.cities_list = function(req,res){
// 	redisClient.hkeys('cities', (err,city_names) => {
// 		if(err) throw err;
// 		res.json(city_names);	
// 	})
// };

// exports.create_new_city = function(req,res){
// 	var new_city = req.body;

// 	console.log(JSON.stringify(new_city));
// 	redisClient.hset('cities',new_city.name,new_city.descption, (err) => {
// 		if(err) throw err;
// 		res.status(201).json(new_city.name);	
// 	});
// }

// /*
// event emitter example for the beginner 
// */
// var events = require('events');
// function Logger(env){
	
// 	this.env = (env ? env : "development");
// 	events.EventEmitter.call(this);
	
// 	this.info = function(){
// 		this.emit('info');
// 	}

// 	this.debug = function(){
// 		this.emit('debug');
// 	}
// } 

// Logger.prototype.__proto__ = events.EventEmitter.prototype;

// var logger = new Logger("production");

// logger.on('info',function(){
// 	console.log("info level");
// });

// logger.on('debug',function(){
// 	if(this.env !== "production"){
// 		console.log("debug level",this.env);	
// 	}
// });

// exports.event_emitter = function(req,res){
// 	logger.info();
// 	logger.debug();
// 	res.send("ok");
// }

// exports.fetch_redis_key = function(req,res){

// }

// function rk(){
// 	return Array.prototype.slice.call(arguments).join("::");
// }

// exports.store_redis_key = function(req,res){
// 	var body = req.body;
// 	console.log("---->",JSON.stringify(req.body));
// 	var base = "employes";
// 	var str_name = "employer";
// 	var name = body.name;
// 	redisClient.hset(rk(base,str_name,name),str_name,body.employer,function(err,rs){
// 		if(err) throw err;
// 		console.log(rk(base,str_name,name),"====");
// 		res.json(rs);	
// 	});
// }


// exports.kafka_publisher = function(req,res){
// 	var body = req.body;
// 	console.log("===========>>",JSON.stringify(body));
// 	kafka_producer_helper.send([{ topic: 'topic1', messages: JSON.stringify(body), partition: 0 }],function(err,status){
// 		if(!err){
			
// 			res.status(200).send("ok");	

// 		}else{
// 			console.log(JSON.stringify(err));
// 			res.status(400).send("some thing went wrong");	
// 		}
// 	});
// }

// exports.create_singleton = function(req,res){
// 	var requestObject = singleton_module.getInstance();
// 	res.json({"object" : JSON.stringify(requestObject)});
// }

// async function pending_func(key){
	
// 	try{
// 		let result1 = await demo(key);
// 		console.log("-------------i will never reach here=========",result1);
// 		return result1;
// 	}catch(err){
// 		console.log("---------------------",err);
// 		throw err;
// 	}
// } 

// function demo(key){
// 	return new Promise(function (resolve,reject){
// 		setTimeout(() => {
// 			console.log("------------print 1");
// 			if(key % 2 == 0){
// 				resolve(key);
// 			}else{
// 				reject(new Error("some thing went wrong"));
// 			}
// 		},1000);
// 	});
// }

// exports.async_awailt_example = function(req,res) {
	
// 	pending_func(3)
// 	.then((result)=>{
// 		console.log("it will not print the message++++++++",result);
// 		res.status(200).send("ok");
// 	})
// 	.catch((err)=>{
// 		console.log("++++++++++finally");
// 		res.status(400).send("some thing went  wrong");
// 	});
		
// }

// exports.ping = function(req,res){
// 	console.log("--------------------from server 1",count);
// 	count++;
// 	res.send("pong from ping");
// };


// subscriber.subscribe("demo");
// subscriber.on("message",function(channel,message){
// 		console.log("--------------+++++++++++",channel,message);
// });

// exports.redis_subscriber = function(req,res){
// 	redisClient.on("message",function(channel,message){
// 		console.log("--------------",channel,message);
// 	});
// 	res.send("ok");
// }

// app.get('/nvm8',exports.nvm_latest); 
// app.get('/cities',exports.cities_list);
// app.post('/cities',urlencode,exports.create_new_city);
// app.get('/event_emitter',exports.event_emitter);
// app.get('/fetch_redis_key',exports.fetch_redis_key);
// app.post('/store_redis_key',urlencode,exports.store_redis_key);
// app.get('/create_singleton',exports.create_singleton);

// app.get('/async_awailt_example',exports.async_awailt_example);

// //kafka routes
// app.post('/kafka_publisher',urlencode,exports.kafka_publisher);
// app.get('/ping',exports.ping);
// // app.get('/async_hook_demo',exports.async_hook_demo);
// // app.get('/kafka_consumer_pause',exports.kafka_consumer_pause);
// // app.get('/kafka_consumer_resume',exports.kafka_consumer_resume);
// // app.get('/kafka_consumer_commit',exports.kafka_consumer_commit)

// app.get("/redis_subscriber",exports.redis_subscriber);

exports.kafka_consumer_start = (req,res) => {
	
}

app.listen(4000);

//module.exports = app;



