counter=1
store_id=$1

echo $store_id

while [ $counter -le 50 ]
do
	
	curl -X POST \
	  http://localhost:5000/kafka_publisher \
	  -H 'app-version: 107' \
	  -H 'brand-id: 3' \
	  -H 'cache-control: no-cache' \
	  -H 'client-os: android' \
	  -H 'client-source: 3' \
	  -H 'content-type: application/json' \
	  -H 'postman-token: bf204ee8-5bf3-200e-be0d-76d2c48bfe3d' \
	  -d '{"order_validate" : "hello kafka"}'

	counter=$((counter+1))
	sleep 3
done