var redis = require('redis');
var rediscall = {};
var client = redis.createClient();

var cache = require('../models/caching.js');
//var cache = models.caching;

rediscall.incr_store_wise_counter =  function(store_id,callback){
	var queue_name = "eod_queue_" + store_id;

	client.exists(queue_name,function(err,data){
		if(!err){
			client.rpush(queue_name,1,function(err,sucess){
				if(!err){
					res.send("sucessfully pushed to the job queue");
					callback();
				}else{
					callback(err);
				}
			});
		}else{
			callback(err);
			//res.send(JSON.stringify(err));
		}
	});	
}

rediscall.get_first_level_cache = function  (req,res) {
	var store_id = req.params.store_id;
	cache.get_first_level_cache(store_id,function(err,response){
		if (!err) {
			res.send(response);
		}else{
			res.send(err);
		}
	});
}

rediscall.get_second_level_cache = function  (req,res) {
	cache.get_second_level_cache(store_id,function(err,response){
		if(!err){
			res.send(response);
		}else{
			res.send(err);
		}
	});
}

rediscall.update_second_level_cache = function(req,res){
	var collection_name = req.params.collection_name;
	var store_id = req.params.store_id;

	var update_collection = {};
	update_collection.collection_body = req.body.collection_body;

	cache.update_second_level_cache(store_id,collection_name,update_collection,function(err,response){
		if(!err){
			res.send(response);
		}else{
			res.send(err);
		}
	});
}
module.exports = rediscall;