var express = require('express');
//var cluster = require('cluster'); 

var app = express();

var cluster = require('cluster');
var numCPUs = require('os').cpus().length;

var bodyParser = require('body-parser');
var urlencode = bodyParser.urlencoded({extended : false});

var redis = require('redis');
var redisClient = redis.createClient();
var singleton_module = require('./controllers/singleton.js');
//redisClient.select((process.env.NODE_ENV || 'development').length);

var kafka_producer_helper = require('./helper/kafka_helper.js');
var kafka_consumer_helper = require('./helper/kafka_consumer_helper.js');

console.log(JSON.stringify(kafka_consumer_helper));

exports.nvm_latest = function(req,res){
	res.send("ok");
}

exports.cities_list = function(req,res){
	redisClient.hkeys('cities', (err,city_names) => {
		if(err) throw err;
		res.json(city_names);	
	})
};

exports.create_new_city = function(req,res){
	var new_city = req.body;

	redisClient.hset('cities',new_city.name,new_city.descption, (err) => {
		if(err) throw err;
		res.status(201).json(new_city.name);	
	});
}

/*
event emitter example for the beginner 
*/
var events = require('events');
function Logger(env){
	
	this.env = (env ? env : "development");
	events.EventEmitter.call(this);
	
	this.info = function(){
		this.emit('info');
	}

	this.debug = function(){
		this.emit('debug');
	}
} 

Logger.prototype.__proto__ = events.EventEmitter.prototype;

var logger = new Logger("production");

logger.on('info',function(){
	console.log("info level");
});

logger.on('debug',function(){
	if(this.env !== "production"){
		console.log("debug level",this.env);	
	}
});

exports.event_emitter = function(req,res){
	logger.info();
	logger.debug();
	res.send("ok");
}

exports.fetch_redis_key = function(req,res){

}

function rk(){
	return Array.prototype.slice.call(arguments).join("::");
}

exports.store_redis_key = function(req,res){
	var body = req.body;
	console.log("---->",JSON.stringify(req.body));
	var base = "employes";
	var str_name = "employer";
	var name = body.name;
	redisClient.hset(rk(base,str_name,name),str_name,body.employer,function(err,rs){
		if(err) throw err;
		console.log(rk(base,str_name,name),"====");
		res.json(rs);	
	});
}


exports.kafka_publisher = function(req,res){
	var body = req.body;
	console.log("===========>>",JSON.stringify(body));
	kafka_producer_helper.send([{ topic: 'demo', messages: JSON.stringify(body), partition: 0 }],function(err,status){
		if(!err){
			
			res.status(200).send("ok");	

		}else{
			console.log(JSON.stringify(err));
			res.status(400).send("some thing went wrong");	
		}
	});
}

exports.kafka_consumer_commit = function(req,res){
	kafka_consumer_helper.commit_flow(function(err,result){
		if(!err){
			console.log("=======>>>>",JSON.stringify(result));
			res.status(200).send("kafka consumer flow commited");
		}else{
			console.log("=======>>",JSON.stringify(err));
			res.status(400).send("not able to commit the flow");
		}
	});
};

exports.kafka_consumer_pause = function(req,res){
	kafka_consumer_helper.pause_flow(function(err){
		if(!err){
			console.log("---------------inside the pause");
			res.status(200).send("kafka_consumer_pause successfully");
		}else{
			res.status(400).send("not able to pause");
		}
	});	
};

exports.kafka_consumer_resume = function(req,res){
	kafka_consumer_helper.resume_flow(function(err){
		if(!err){
			res.status(200).send("kafka_consumer_resume successfully");
		}else{
			res.status(400).send("not able to resume the flow");
		}
	});
};


exports.create_singleton = function(req,res){
	var requestObject = singleton_module.getInstance();
	res.json({"object" : JSON.stringify(requestObject)});
}
// if(cluster.isMaster){
// 	for(let i = 0; i < numCPUs; i++){
// 		cluster.fork();
// 	}

// 	Object.keys(cluster.workers).forEach(function (id) {
//     	console.log("ecma running with ID : " + cluster.workers[id].process.pid);
//     	//logger.info("ecma running with ID : " + cluster.workers[id].process.pid); //send to papertrailt too
//   	});

// 	cluster.on('exit', function (worker, code, signal) {
//       console.log('worker ' + worker.process.pid + ' died');
//       logger.info('worker ' + worker.process.pid + ' died');//send to papertrailt too
//     });

// }else{
// 	console.log("inside the else part");
// 	app.get('/nvm8',exports.nvm_latest); 
// }

// process.on('exit',()=>{
// 	console.log("=============process on exit");
// })

// process.on('uncaughtException',(err)=>{
// 	console.log("-------------uncaughtException",JSON.stringify(err));
// 	if (cluster.worker.isConnected() && !cluster.worker.isDead()) {
//       cluster.worker.disconnect();
//     }
// });



app.get('/nvm8',exports.nvm_latest); 
app.get('/cities',exports.cities_list);
app.post('/cities',urlencode,exports.create_new_city);
app.get('/event_emitter',exports.event_emitter);
app.get('/fetch_redis_key',exports.fetch_redis_key);
app.post('/store_redis_key',urlencode,exports.store_redis_key);
app.get('/create_singleton',exports.create_singleton);

//kafka routes
app.post('/kafka_publisher',urlencode,exports.kafka_publisher);

app.get('/kafka_consumer_pause',exports.kafka_consumer_pause);
app.get('/kafka_consumer_resume',exports.kafka_consumer_resume);
app.get('/kafka_consumer_commit',exports.kafka_consumer_commit)

app.listen(8000,()=>{
	console.log('consumer-1 is has been started on port 8000');
});
//module.exports = app;



