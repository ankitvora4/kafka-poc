var kafka_helper = (function (argument) {

	var kafka = require('kafka-node');
	var Producer = kafka.Producer;
	var client = new kafka.Client();
	var producer = new Producer(client);

	producer.on('ready',function(err,result){
		if(!err){
			//console.log("--------------->> kafka producer is up and running");	
		}else{
			throw err;
		}
	});

	producer.on('error',function(err){
		console.log("----------------------------------------------------");
		throw err;
	});

	return producer;

})();


module.exports = kafka_helper;