var singleton = (function(){

	var singleInstance;

	function createInstance(){
		console.log("does it happened");
		var instance = new Object("singleton is here");
		return instance;
	}

	return {

		getInstance : function(){
			console.log("---------->>>");
			if(singleInstance){
				return singleInstance;
			}

			singleInstance = createInstance();
			return singleInstance;
		}
	};

})();


module.exports = singleton;





