var app = require('./app');

var request = require('supertest');

var redis = require('redis');
var redisClient = redis.createClient();
redisClient.select('test'.length);
redisClient.flushdb();

describe("request to /nvm8 path", ()=> {
	it('returns 200 status code', (done) => {
		request(app)
		.get('/nvm8')
		.expect(200)
		.end((error) => {
			if(error) throw error;
			done();
		});		
	});

	it('returns html file as response', (done) => {
		request(app)
			.get('/nvm8')
			.expect('Content-Type',/html/,done)
	});
});


describe('request to /cities', () => {
	it('returns status code 200', (done) => {
		request(app)
			.get('/cities')
			.expect(200,done);
	});

	it('returns json as response', (done) => {
		request(app)
			.get('/cities')
			.expect('Content-Type',/json/ , done)
	});

	it('initial cities in response', (done) => {
		request(app)
			.get('/cities')
			.expect(JSON.stringify([]),done);
	});
});

describe('creating new city', () => {
	it('returns status code 201' , (done) => {
		request(app)
			.post('/cities')
			.send('name=Bhavnagar&description=where+the+ankit+lives')
			.expect(201,done)
	});

	it('return the city name in response', (done) => {
		request(app)
			.post('/cities')
			.send('name=Bhavnagar&description=where+the+ankit+lives')
			.expect(/Bhavnagar/i,done);
	});
	
});
