var kafka_consumer_helper = (function () {

	var kafka = require('kafka-node');
	var HighLevelConsumer = kafka.HighLevelConsumer;
	var client = new kafka.Client();
	var consumer = new HighLevelConsumer(client,[
			{
				"topic" : "demo", "partition" : 1
			}
		], 
		{
			"groupId" : 'test_group',
			//'autoCommit' : true
		}
	);
	var counter = 0;

	consumer.addTopics([{
				"topic" : "demo", "offset" : 2, "partition" : 1
	}],function(err,status){
		if(!err){
			console.log(JSON.stringify(status));
		}else{
			console.log(JSON.stringify(err));
		}
	},true);

	consumer.on("error",function(err){
		console.log("--------------errr",JSON.stringify(err));
	});


	consumer.on('message',function(msg){
		counter++;
		console.log("------message",JSON.stringify(msg),counter);

	});

	console.log("------------> kafka consumer is up and running <---------------");
	
	function pause_flow(callback){
		console.log("------helper pause");
		consumer.pause();
		//console.log(result);
		callback(null);
	}

	function resume_flow(callback){
		consumer.resume();
		callback(null);

	}

	function commit_flow(cb){
		consumer.commit(cb);
	}

	return {
		pause_flow,
		resume_flow,
		commit_flow
	};

})();

module.exports = kafka_consumer_helper;