curl -X POST \
  http://localhost:5000/kafka_publisher \
  -H 'app-version: 107' \
  -H 'brand-id: 3' \
  -H 'cache-control: no-cache' \
  -H 'client-os: android' \
  -H 'client-source: 3' \
  -H 'content-type: application/json' \
  -H 'postman-token: bf204ee8-5bf3-200e-be0d-76d2c48bfe3d' \
  -d '{"order_validate" : "RIP BLACK PANTHER"}'